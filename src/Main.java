import java.util.Date;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.util.Eval;


public class Main
{
	public static void main(String[] args)
	{
		System.out.println(Eval.me("'foo Hello'.toUpperCase()"));

		Binding sharedData = new Binding();
		GroovyShell shell = new GroovyShell(sharedData);
		System.out.println(shell.evaluate("'foo Hello'.toUpperCase()"));

		Date now = new Date();
		sharedData.setProperty("text", "I am shared data!");
		sharedData.setProperty("date", now);

		System.out.println(shell.evaluate("\"At $date, $text\""));
	}
}
